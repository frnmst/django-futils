Installation
============

Although you can install django-futils as a normal Python package, running
the app is only supported on Docker.

Distribution files
------------------

django-futils uses distribution (ending with ``dist``) files for some configurations.
These files and are example configurations. What you need to do is to copy them
as new files without the ``.dist`` suffix and edit them as you like:

- ``./docker-compose.yml.dist``
- ``./SECRET_SETTINGS.py.dist``
- ``./env.dist``
- ``./docker/docker-compose.dev.yml.dist``
- ``./docker/docker-compose.prod.yml.dist``
- ``./docker/docker-compose.serve_dev.yml.dist``
- ``./docker/docker-compose.serve_prod.yml.dist``
- ``./docker/docker-compose.test_dev.yml.dist``

.. important:: Once everything is installed and working generate a new secret key and replace the once in the ``./SECRET_SETTINGS.py`` file. Run:


  ::


      pipenv run python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'



Makefile and uwsgi.ini
----------------------

Follow these instructions to download some required files:

-  https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django/src/branch/master#other-files

Prerequsites
------------

Install these packages from your package manager:

+----------------------+---------------------+------------------+
| Name                 | Binaries            | Version          |
+======================+=====================+==================+
| GNU make             | - make              | 4.3              |
+----------------------+---------------------+------------------+
| Docker               | - docker            | >= 19.03.0       |
+----------------------+---------------------+------------------+
| Python               | - python3           | 3.8              |
+----------------------+---------------------+------------------+

Install these packages from PyPI:

+----------------------+---------------------+------------------+
| Name                 | Binaries            | Version          |
+======================+=====================+==================+
| docker-compose       | - docker-compose    | >= 1.25.5        |
+----------------------+---------------------+------------------+
| pipenv               | - pipenv            |                  |
+----------------------+---------------------+------------------+

Debian GNU/Linux 10 and 11
``````````````````````````

These commands will help having everything working, including
building the documentation on Debian GNU/Linux:


::


    $ pip3 install --user pipenv docker-compose
    $ make install-dev


Start and enable the Docker service:


::


    # systemctl start docker
    # systemctl enable docker


A new user, ``postgis-docker``, needs to be created to run the app.
Moreover, all the needed directories must be created **before** running the app.

::


    # usermod -u 998 systemd-coredump
    # groupmod --gid 997 systemd-coredump
    # useradd -m -s /bin/bash -u 999 -U postgis-docker
    # mkdir -p /home/postgis-docker/django-futils/{dev,prod}/data
    # mkdir -p ./db/{dev,prod}/data
    # chown -R postgis-docker:postgis-docker /home/postgis-docker/django-futils
    # chown -R ${developer}:${developer_group} ./db
    # chmod 700 -R /home/postgis-docker/django-futils
    # chmod 700 -R ./db

Finally, run ``make docker.build.dev`` or ``make docker.build.prod`` depending on what you have to do.

Default credentials and variables
---------------------------------

+---------------------------+---------------------+
| Description               | Value               |
+===========================+=====================+
| Django admin user         | ``admin``           |
+---------------------------+---------------------+
| Django admin password     | ``adminpassword``   |
+---------------------------+---------------------+
| Postgres user             | ``postgres``        |
+---------------------------+---------------------+
| Postgres password         | ``postgres``        |
+---------------------------+---------------------+

.. warning:: Change the django credentials immediately! The Django admin user is infact a superuser.

.. important:: You can change some of the docker-compose variables in the ``./.env`` file.
