/*
 * Jenkinsfile
 *
 * Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
 *
 * This file is part of django-futils.
 *
 * django-futils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * django-futils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with django-futils.  If not, see <http://www.gnu.org/licenses/>.
 */

pipeline {
    agent any

    environment {
        CP = """${sh(
            returnStdout: true,
            script: 'which cp'
        )}"""
        CURL = """${sh(
            returnStdout: true,
            script: 'which curl'
        )}"""
        PYTHON3 = """${sh(
            returnStdout: true,
            script: 'which python3'
        )}"""
        DOCKER_COMPOSE = """${sh(
            returnStdout: true,
            script: 'which docker-compose'
        )}"""
        MAKE = """${sh(
            returnStdout: true,
            script: 'which make'
        )}"""
        AWK = """${sh(
            returnStdout: true,
            script: 'which awk'
        )}"""
        SHA512SUM= """${sh(
            returnStdout: true,
            script: 'which sha512sum'
        )}"""
        ID = """${sh(
            returnStdout: true,
            script: 'which id'
        )}"""
        PYPI_MIRROR = """${sh(
            returnStdout: true,
            script: 'echo -n "https://pypi.franco.net.eu.org/simple"'
        )}"""
    }
    stages {
        stage('Build') {
            when {
                    expression { BRANCH_NAME ==~ /(development-ci|production-ci)/ }
            }
            environment {
                // Variables.
                MAKEFILE_URL = "https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django/raw/branch/dev/Makefile.dist"
                MAKEFILE_CHECKSUM = "3993c5ba5b94f4732b67be7ff70217ba5f4fffd14d5b7f17cf35ea88d4c4de26445368763f25e397f22b9762e47d130cebb644aa24a6405e65fbf8e82758a2d3"
                UWSGI_INI_URL = "https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django/raw/branch/dev/uwsgi.ini.dist"
                UWSGI_INI_CHECKSUM = "0d9a868f911a77105cd98140eaa9652f4dcabf1539c13adf7344d00c8164eb1112c310f8e53ce2a43fc82607161b5da8690314acd1fc0a88794b98a4b1a17148"

                BASE_CI_DIR = "${env.WORKSPACE}/ci"
                PYENV_HOME = "${env.BASE_CI_DIR}/python_env"
                PIPENV_CACHE_DIR = "${env.BASE_CI_DIR}/pipenv_cache"
                WORKON_HOME = "${env.BASE_CI_DIR}/pipenv_venv_home"
                PIP_DOWNLOAD_CACHE = "${env.BASE_CI_DIR}/pip_download_cache"
                PIP_CACHE_DIR = "${env.BASE_CI_DIR}/pip_cache"
            }
            steps {
                echo 'Building...'
                sh '''
                    cd docker
                    ln -s docker-compose.dev.yml.dist docker-compose.dev.yml
                    ln -s docker-compose.prod.yml.dist docker-compose.prod.yml
                    ln -s docker-compose.serve_dev.yml.dist docker-compose.serve_dev.yml
                    ln -s docker-compose.serve_prod.yml.dist docker-compose.serve_prod.yml
                    ln -s docker-compose.test_dev.yml.dist docker-compose.test_dev.yml
                    cd ..
                    ${CP} docker-compose.yml.dist docker-compose.yml
                    ${CP} SECRET_SETTINGS.py.dist SECRET_SETTINGS.py
                    ${CP} env.dist .env
                    ${PYTHON3} -m venv --clear --without-pip "${PYENV_HOME}"
                    bash -c "source "${PYENV_HOME}"/bin/activate"
                    ${CURL} https://bootstrap.pypa.io/get-pip.py -o get-pip.py
                    python3 get-pip.py
                    ~/.local/bin/pip3 install pipenv
                '''

                // Download files from docker-debian-postgis-django.
                sh '''
                    ${CURL} "${MAKEFILE_URL}" --output Makefile && [ "$(${SHA512SUM} Makefile | ${AWK} '{print $1}')" = "${MAKEFILE_CHECKSUM}" ]
                    ${CURL} "${UWSGI_INI_URL}" --output uwsgi.ini && [ "$(${SHA512SUM} uwsgi.ini | ${AWK} '{print $1}')" = "${UWSGI_INI_CHECKSUM}" ]
                '''
                script {
                    sh '~/.local/bin/pipenv lock --requirements > requirements.txt'
                    if (env.BRANCH_NAME == 'development-ci') {
                        // Replace the pinned version of docker-debian-postgis-django with 'dev'.
                        sh '''
                            sed -i "s/docker-debian-postgis-django.git#[0-9]\\+\\.[0-9]\\+\\.[0-9]\\+/docker-debian-postgis-django.git#dev/g" docker-compose.yml
                            ~/.local/bin/pipenv lock --requirements --dev >> requirements.txt
                            ${DOCKER_COMPOSE} build --force-rm --no-cache --memory=2GB --build-arg DJANGO_ENV=development --build-arg GID=$(${ID} -g) --build-arg UID=$(${ID} -u) --build-arg PYPI_MIRROR=${PYPI_MIRROR}
                        '''
                    } else {
                        sh '${DOCKER_COMPOSE} build --force-rm --no-cache --memory=2GB --build-arg DJANGO_ENV=production --build-arg GID=$(${ID} -g) --build-arg UID=$(${ID} -u) --build-arg PYPI_MIRROR=${PYPI_MIRROR}'
                    }
                }
            }
        }
        stage('Init and test') {
            when {
                    expression { BRANCH_NAME ==~ /(development-ci|production-ci)/ }
            }
            steps {
                echo 'Init and test...'
                script {
                    if (env.BRANCH_NAME == 'development-ci') {
                        // Do not enable named volumes and delete anonymous volumes when finished.
                        sh '${DOCKER_COMPOSE} --file docker-compose.yml --file docker/docker-compose.debug.yml --file docker/docker-compose.init_dev.yml --file docker/docker-compose.db_name_dev.yml up --always-recreate-deps --renew-anon-volumes --abort-on-container-exit --remove-orphans --exit-code-from web web'
                    } else {
                        sh '${DOCKER_COMPOSE} --file docker-compose.yml --file docker/docker-compose.no_debug.yml --file docker/docker-compose.init_prod.yml --file docker/docker-compose.db_name_prod.yml up --always-recreate-deps --renew-anon-volumes --abort-on-container-exit --remove-orphans --exit-code-from web web'
                    }
                }
            }
        }
        stage('Teardown') {
            when {
                    expression { BRANCH_NAME ==~ /(development-ci|production-ci)/ }
            }
            steps {
                echo 'Teardown...'
                sh '''
                    ${DOCKER_COMPOSE} down --volumes --remove-orphans
                '''
            }
        }
    }
    post {
        always {
            cleanWs()

            // Remove dangling images.
            sh 'docker image prune --force'
        }
    }
}
