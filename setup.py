#
# setup.py
#
# Copyright (C) 2017-2020 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of django-futils.
#
# django-futils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# django-futils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with django-futils.  If not, see <http://www.gnu.org/licenses/>.
#

from setuptools import find_packages, setup

setup(
    name='django_futils',
    version='5.0.0',
    include_package_data=True,
    packages=find_packages(exclude=['*tests*']),
    license='GPL',
    description='A set of models, an admin and utilities for frequently used patterns.',
    long_description=open('README.rst').read(),
    long_description_content_type='text/x-rst',
    author='Franco Masotti',
    author_email='franco.masotti@tutanota.com',
    keywords='django utilities',
    url='https://blog.franco.net.eu.org/software/#django-futils',
    python_requires='>=3.9, <4',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Utilities',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Intended Audience :: Developers',
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Framework :: Django',
        'Framework :: Django :: 3.2',
    ],
    install_requires=[
        'Django>=3.2,<3.3',
        'uwsgi>=2.0,<2.1',
        'requests>=2.26,<2.27',
        'django-countries>=7,<8',
        'django-phone-field>=1,<2',
        'django-vies>=6,<7',
        'django-simple-history>=3,<4',
        'django-cleanup>=5,<6',
        'django-import-export>=2,<3',
        'psycopg2>=2.9,<2.10',
        'django-extensions>=3,<4',
        'django-leaflet>=0.28,<0.29',
        'django-htmlmin>=0.11,<0.12',
        'geopy>=2,<3',
        'django-redis>=5,<6',
    ],
)
