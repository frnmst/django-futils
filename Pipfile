#
# Pipfile
#
# Copyright (C) 2020-2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of docker-debian-postgis-django.
#
# docker-debian-postgis-django is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# docker-debian-postgis-django is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with docker-debian-postgis-django.  If not, see <http://www.gnu.org/licenses/>.

[[source]]
name = "pypi"
url = "https://pypi.org/simple"
verify_ssl = true

[dev-packages]
pre-commit = ">=2,<3"

[packages]
# Documentation.
Sphinx = ">=4,<5"
sphinx-rtd-theme = ">=1,<2"

# Tools.
pygraphviz = ">=1,<2"

# Tests.
model-bakery = ">=1.3,<2"

########
# Base #
########
Django = ">=3.2,<3.3"
uwsgi = ">=2.0,<2.1"
requests = ">=2"

# Models.
django-countries = ">=7,<8"
django-phone-field = ">=1,<2"
django-vies = ">=6,<7"
## Model history.
django-simple-history = ">=3,<4"
## Remove dangling files.
django-cleanup = ">=5,<6"
django-import-export = ">=2,<3"

# Postgres.
# If you prefer to avoid building psycopg2 from source, please install the PyPI
# 'psycopg2-bin' package instead.
# See https://www.psycopg.org/docs/install.html#psycopg-vs-psycopg-binary
psycopg2 = ">=2.9,<2.10"

# Tools.
django-extensions = ">=3,<4"
django-leaflet = ">=0.28,<0.29"
django-htmlmin = ">=0.11,<0.12"
geopy = ">=2,<3"
django-redis = ">=5,<6"
